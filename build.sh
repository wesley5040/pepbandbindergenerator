#!/bin/bash

# This builds the parts and cleans up
#
# Usage:
# ./build.sh -a       # To build all parts (in parallel)
# ./build.sh -c       # To clean up all the crap
# ./build.sh -p file  # To build one part (like 01-flute1.tex)
#
# Note that the output from building them all at once is basically garbage, so
# just look at the .log files that are generated if you care about that output.
# I just left so you know it's doing something since it takes a while.
#
# Wesley Van Pelt, January 2019

OPTIND=1

mkdir -p pdfs
cd pdfs

while getopts "acp:" opt; do
	case "$opt" in
	a)	pdflatex ../tex/01-flute1.tex &
		pdflatex ../tex/01-flute2.tex &
		pdflatex ../tex/02-clarinet1.tex &
		pdflatex ../tex/02-clarinet2.tex &
		pdflatex ../tex/03-altosax1.tex &
		pdflatex ../tex/03-altosax2.tex &
		pdflatex ../tex/04-tenorsax.tex &
		pdflatex ../tex/06-horn.tex &
		pdflatex ../tex/07-trumpet1.tex &
		pdflatex ../tex/07-trumpet2.tex &
		pdflatex ../tex/07-trumpet3.tex &
		pdflatex ../tex/08-trombone1.tex &
		pdflatex ../tex/08-trombone2.tex &
		pdflatex ../tex/08-trombone3.tex &
		pdflatex ../tex/09-baritonebc.tex &
		pdflatex ../tex/09-baritonetc.tex &
		pdflatex ../tex/09-tuba.tex &
		pdflatex ../tex/10-bass.tex
		wait
		;;
	c)	rm -f *.log *.aux
		;;
	p)	pdflatex "../tex/$OPTARG"
		;;
	esac
done
