# Pepband Binder Generator

Pep band music binder generator made for the Rose-Hulman Pep Band.

This just uses a bunch of LaTeX files to generate a single PDF for each instrument.  This assumes you have the `parts` directory in the root of the repo with all of the music in it.  I'm not putting it here since it's multiple gigabytes.
